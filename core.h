#ifndef CORE_H
#define CORE_H

#include <QObject>
#include <QFile>
#include "ftp.h"
#include "connectionmodel.h"

class Core : public QObject
{
    Q_OBJECT

    Q_PROPERTY(QString host READ host WRITE setHost NOTIFY hostChanged)
    Q_PROPERTY(QString user READ user WRITE setUser NOTIFY userChanged)
    Q_PROPERTY(QString password READ password WRITE setPassword NOTIFY passwordChanged)
    Q_PROPERTY(QString status READ status NOTIFY statusChanged)
    Q_PROPERTY(QString localPath READ localPath WRITE setLocalPath NOTIFY localPathChanged)
    Q_PROPERTY(ConnectionModel * connectionModel READ connectionModel CONSTANT)
public:

    static Core &instance();

    QString host() const { return m_host; }
    void setHost(QString);

    QString user() const { return m_user; }
    void setUser(QString val);

    QString password() const { return m_pass; }
    void setPassword(QString val);

    QString status() const { return m_status; }

    QString localPath() const { return m_localPath; }
    void setLocalPath(QString val);

    ConnectionModel * connectionModel() { return &m_connModel; }

    Q_INVOKABLE void connectToHost();
    Q_INVOKABLE void get(QString name);
    Q_INVOKABLE void send(QString name);
    Q_INVOKABLE void addConnection();

    Ftp &ftp() { return m_ftp; }

private:

    Core();

    Ftp m_ftp;
    QString m_host;
    QString m_user;
    QString m_pass;
    QString m_status;
    QString m_localPath;
    QSharedPointer<QFile> m_file;
    ConnectionModel m_connModel;

    void setStatus__(QString val);

private slots:

    void onFtpCommandFinished(FtpCommand c, FtpStatus status);

signals:

    void currentPathChanged();
    void hostChanged();
    void userChanged();
    void passwordChanged();
    void statusChanged();
    void localPathChanged();

    void connected(bool val);
    void finished();
};

#endif // CORE_H
