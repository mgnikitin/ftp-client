#include "urlinfo.h"

UrlInfo::UrlInfo()
{

}

void UrlInfo::setType(Type val) {

    m_type = val;
}

void UrlInfo::setName(QString val) {

    m_name = val;
}

void UrlInfo::setOwner(QString val) {

    m_owner = val;
}

void UrlInfo::setGroup(QString val) {

    m_group = val;
}

void UrlInfo::setSize(qint64 val) {

    m_size = val;
}

QString UrlInfo::toString() const {

    return QString("%1").arg(m_name);
}
