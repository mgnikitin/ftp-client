#ifndef FTPSTATUS_H
#define FTPSTATUS_H

#include <QMetaType>

enum class FtpStatus {

    Ok,
    LoginIncorrect, // only for login command
    FileUnavailable,
    Error
};

Q_DECLARE_METATYPE(FtpStatus)

enum class FtpDtpMode {
    None,
    FileRead,
    FileWrite,
    List
};

Q_DECLARE_METATYPE(FtpDtpMode)

#endif // FTPSTATUS_H
