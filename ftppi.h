#ifndef FTPPROTO_H
#define FTPPROTO_H

#include "ftpconn.h"
#include "ftpcommand.h"
#include "ftpdtp.h"
#include "ftpstatus.h"

/** \brief Управляющее соединение с ftp-сервером
  */
class FtpPi : public FtpConn
{
    Q_OBJECT
public:

    FtpPi();

    void abort();

    bool sendCommand(const FtpCommand &cmd_);

    const FtpCommand &lastCommand() const { return m_lastCmd; }

private:

    enum State : unsigned char {
        Waiting = 1,
        Success,
        Idle,
        Failure
    };

    enum WaitFlag : unsigned char {
        WaitNo = 0,
        WaitDtpConnect = 0x01,
        WaitDtpDisconnect = 0x02,
        WaitConnect = 0x04,
        WaitReply = 0x08,
        WaitDtpTask = 0x10
    };

    QStringList m_cmds;
    FtpCommand m_lastCmd;
    QString m_replyText;
    char m_replyCode[3];
    unsigned char m_waitFlags;
    FtpDtp::State m_lastDtpState;
    bool m_finished;
    State m_state;

    void nextCommand();
    bool decodeAddress(QString line, QString &host, quint16 &port) const;
    bool decode(const QString &line);

    void clear__(FtpStatus s);

    bool verifyCode(const QString &line);

public slots:

    void onDtpState(FtpDtp::State);
    void onDtpFinished(bool success);

private slots:

    void onReadyRead();
    void onError(QAbstractSocket::SocketError);

signals:

    void finished(FtpStatus);

    // dtp
    void dtpConnect(QString, quint16);
    void dtpDisconnect();
    void dtpWrite();
    void dtpMode(FtpDtpMode);
    void dtpClearList();
    void dtpFileSize(size_t);
};

#endif // FTPPROTO_H
