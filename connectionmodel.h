#ifndef CONNECTIONMODEL_H
#define CONNECTIONMODEL_H

#include <QAbstractListModel>
#include "sqlhelper.h"

class ConnectionModel : public QAbstractListModel {
    Q_OBJECT
public:

    enum Roles {
        ConnNameRole = Qt::UserRole + 1,
        HostRole,
        UserRole,
        PassRole
    };

    Q_ENUMS(Roles)

    struct Item {
        QString name;
        QString host;
        QString user;
        QString pass;
    };


    ConnectionModel();

    int rowCount(const QModelIndex &parent = QModelIndex()) const { if(parent.isValid()) return 0; return m_data.size(); }
    QVariant data(const QModelIndex &index, int role) const;
    bool removeRows(int position, int rows, const QModelIndex &parent = QModelIndex());
    Q_INVOKABLE void clear();
    void add(const Item &val);

    QHash<int, QByteArray> roleNames() const;

private:

    QSqlDatabase m_db;
    QList<Item> m_data;
    QMap<QString, int> m_map;

    ConnectionModel(const ConnectionModel &) = delete;
    ConnectionModel &operator =(const ConnectionModel &) = delete;

    void fill(const QVector<QVariantList> &vec);

};

#endif // CONNECTIONMODEL_H
