import QtQuick 2.0
import QtQuick.Controls 2.0
import QtQuick.Layouts 1.0
import FtpFilesModel 1.0
import BrowseFileHelper 1.0

Page {
    id: root

    BrowseFileHelper {
        id: helper
    }

    Component.onCompleted: {
        currentPathField.focus = true
    }

    header:  ToolBar {
        RowLayout {
            anchors.fill: parent
            ToolButton {
                id: up
                text: qsTr("Up")

                onClicked: { files.dirUp() }
            }

            TextField {
                id: currentPathField
                Layout.fillWidth: true

                onAccepted: { files.folder = helper.localStringToUrl(text) }
            }
        }
    }

    FilesView {
        id: files
        anchors.fill: parent
        model: FtpFilesModel {}
        selectMultiple: true

        onFolderChanged: {
            console.log("folder " + folder)
            currentPathField.text = helper.urlToString(folder)
        }
    }

    footer: ToolBar {

        RowLayout {
            anchors.fill: parent
            anchors.leftMargin: 5
            spacing: 10

            Button {
                text: qsTr("Download")
                enabled: files.selectedIndices.length  !== 0

                onClicked: {
                    var fls = []
                    files.selectedIndices.map(function(idx) {

                        fls.push(files.get(idx, "fileName"))
                    })
                    if(fls.length !== 0) {
                        Core.get(fls[0])
                    }
                }
            }

            Label {
                text: Core.status
                Layout.alignment: Qt.AlignLeft | Qt.AlignVCenter
                Layout.fillWidth: true
            }
        }
    }

    Connections {
        target: Core
        onFinished: {
            if(files.selectedIndices.length !== 0) {
                files.clearIndex(files.selectedIndices[0])
            }
        }
    }
}
