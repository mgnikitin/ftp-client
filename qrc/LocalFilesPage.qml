import QtQuick 2.0
import QtQuick.Controls 2.0
import QtQuick.Layouts 1.0
import Qt.labs.folderlistmodel 2.1
import BrowseFileHelper 1.0

Page {
    id: root

    BrowseFileHelper {
        id: helper
    }

    Component.onCompleted: {
        currentPathField.focus = true
    }

    header:  ToolBar {
        RowLayout {
            anchors.fill: parent
            ToolButton {
                id: up
                text: qsTr("Up")

                onClicked: {
                    files.dirUp()
                }
            }

            TextField {
                id: currentPathField
                Layout.fillWidth: true

                onAccepted: { files.folder = helper.localStringToUrl(text) }
            }
        }
    }

    FilesView {
        id: files
        anchors.fill: parent
        model: FolderListModel {}
        selectMultiple: true

        onFolderChanged: {
            currentPathField.text = helper.urlToString(folder)
            Core.localPath = currentPathField.text
        }

        Component.onCompleted: {
            files.folder = helper.localStringToUrl(Core.localPath)
        }
    }

    footer: ToolBar {

        RowLayout {
            anchors.fill: parent
            anchors.leftMargin: 5
            spacing: 10

            Button {
                text: qsTr("Upload")
                enabled: files.selectedIndices.length  !== 0

                onClicked: {
                    var fls = []
                    files.selectedIndices.map(function(idx) {

                        fls.push(files.get(idx, "fileName"))
                    })
                    if(fls.length !== 0) {
                        Core.send(fls[0])
                    }
                }
            }
        }
    }
}
