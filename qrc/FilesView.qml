import QtQuick 2.0
import QtQuick.Controls 2.0
import QtQuick.Layouts 1.0
import Qt.labs.folderlistmodel 2.1

Item {
    id: root

    // выбранные файлы
    property var files: []
    // выбранные индексы
    property var selectedIndices: []
//    property int lastClickedIdx: -1
    // текущая директория (alias view.model.folder)
    property string folder
    property bool selectMultiple: false
    property alias model: view.model

    Component.onCompleted: {
        if(root.model) {
            root.model.folderChanged.connect(view.setFolder)
        }
    }

    onFolderChanged: {
        clearSelection()
        view.model.folder = folder
    }

    // Перейти в поддиректорию
    function dirDown(path) {
        view.model.folder = path
        selectedIndices = []
//        lastClickedIdx = -1
    }

    // Подняться вверх
    function dirUp() {
        if(view.model.parentFolder.toString() !== "") {
            view.model.folder = view.model.parentFolder
            selectedIndices = []
//            lastClickedIdx = -1
        }
    }

    // Очистить выделенное
    function clearSelection() {
        files = []
    }

    // Добавить выделенный файл
    function addSelection(str) {
        files = [str]
    }

    // Применить выделенное
//    function acceptSelection() {
//        clearSelection()
//        if(selectFolder && selectedIndices.length == 0)
//            addSelection(folder)
//        else {
//            selectedIndices.map(function(idx) {
//                if(view.model.isFolder(idx)) {
//                    if(selectFolder) { addSelection(view.model.get(idx, "filePath")) }
//                }
//                else {
//                    if(!selectFolder) { addSelection(view.model.get(idx, "filePath")) }
//                }
//            })
//        }
//    }

    function addIndex(idx) {
        var selCopy = selectedIndices
        var existingIdx = selCopy.indexOf(idx)
        if(existingIdx >= 0) {
            selCopy.splice(existingIdx, 1)
        }
        else {
            selCopy.push(idx)
        }
        selectedIndices = selCopy
    }

    function clearIndex(idx) {
        console.log("clearIndex " + idx)
        var selCopy = selectedIndices
        var existingIdx = selCopy.indexOf(idx)
        if(existingIdx >= 0) {
            selCopy.splice(existingIdx, 1)
            selectedIndices = selCopy
        }
    }

    function get(idx, prop) {
        return view.model.get(idx, prop)
    }

    Rectangle {
        anchors.fill: parent
//        color: "white"
        radius: 5

        ListView {
            id: view
            anchors.fill: parent
            anchors.margins: 5
            clip: true
            delegate: folderDelegate

            onModelChanged: {
                if(model) {
                    model.folderChanged.connect(setFolder)
                }
            }

            function setFolder() {
                console.log("folder changed " + model.folder)
                root.folder = model.folder
            }
        }
    }

    Component {
        id: folderDelegate

        Rectangle {
            id: wrapper
            width: root.width
            height: nameText.implicitHeight * 1.5
            color: "transparent"

            function launch() {
                if(view.model.isFolder(index)) {
                    dirDown(fileURL)
                }
            }

            Rectangle {
                id: itemHighlight
                visible: root.selectedIndices.indexOf(index) >= 0
                anchors.fill: parent
                color: "#039BE5"
            }

            Image {
                id: icon
                source: "qrc:/qrc/images/folder.png"
                height: wrapper.height - y * 2;
                width: height
                x: 2
                y: 2
                visible: view.model.isFolder(index)
            }

            Item {
                id: contentRow
                anchors.top: parent.top
                anchors.bottom: parent.bottom
                anchors.right: parent.right
                anchors.left: icon.right

                Text {
                    id: nameText
                    anchors.top: parent.top
                    anchors.bottom: parent.bottom
                    anchors.left: parent.left
                    anchors.right: sizeText.visible ? sizeText.left : parent.right
                    anchors.leftMargin: 2
                    anchors.rightMargin: 10
                    verticalAlignment: Text.AlignVCenter
                    text: fileName
                    elide: Text.ElideRight
                }

                Text {
                    id: sizeText
                    visible: !view.model.isFolder(index) && contentWidth < Math.abs(contentRow.width-50)
                    anchors.top: parent.top
                    anchors.bottom: parent.bottom
                    anchors.right: parent.right

                    anchors.rightMargin:  2
                    verticalAlignment: Text.AlignVCenter
                    text: fileSize.toString()
                    color: "#666"
                }
            }

            MouseArea {
                anchors.fill: parent
                onDoubleClicked: {
                    selectedIndices = [index]
//                    root.lastClickedIdx = index
                    launch()
                }

                onClicked: {
                    view.currentIndex = index
                    if (root.selectMultiple) {
                        if(!view.model.isFolder(index)) {
                            addIndex(index)
                        }
                    }
                    else {
                        selectedIndices = [index]
//                        root.lastClickedIdx = index
                    }
                }
            }
        }
    }
}
