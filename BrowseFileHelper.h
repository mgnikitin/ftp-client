#ifndef BROWSEFILEHELDER_H
#define BROWSEFILEHELDER_H

#include <QString>
#include <QObject>

class BrowseFileHelper : public QObject {
    Q_OBJECT
public:

    BrowseFileHelper();

    Q_INVOKABLE QString startPath() const;

    Q_INVOKABLE QString localStringToUrl(QString str) const;
    Q_INVOKABLE QString urlToString(QString url) const;

    Q_INVOKABLE QString pathToUrl(QString text) const;

};

#endif // BROWSEFILEHELDER_H
