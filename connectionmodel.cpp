#include "connectionmodel.h"
#include "config.h"
#include <QSqlError>
#include <QDebug>

//-----------------------------------------------------------------------------
ConnectionModel::ConnectionModel() {

    m_db = QSqlDatabase::addDatabase("QSQLITE");
    m_db.setDatabaseName(Config::instance().databaseFile());

    if(!m_db.open()) {
        qWarning() << m_db.lastError().text();
        return;
    }

    SQLHelper helper;
    if(helper.createTableIfNotExists(m_db)) {
        if(helper.createIndexIfNotExists(m_db, "name")) { // TODO
            QVector<QVariantList> vec;
            if(helper.read(m_db, vec)) {
                fill(vec);
            }
        }
    }
}

//-----------------------------------------------------------------------------
QVariant ConnectionModel::data(const QModelIndex &index, int role) const {

    if(!index.isValid())
        return QVariant();

    if((index.row() < 0) || (index.row() >= m_data.size()))
        return QVariant();

    switch(role) {
    case Qt::DisplayRole:
    case ConnNameRole: return m_data.at(index.row()).name;
    case HostRole: return m_data.at(index.row()).host;
    case UserRole: return m_data.at(index.row()).user;
    case PassRole: return m_data.at(index.row()).pass;
    default:
        return QVariant();
    }
}

//-----------------------------------------------------------------------------
bool ConnectionModel::removeRows(int position, int rows, const QModelIndex &parent) {

    if((rows < 1) || (position < 0) || ((position + rows) > m_data.size()))
        return false;
    beginRemoveRows(parent, position, qMax(0, position+rows-1));

    for (int row = 0; row < rows; ++row) {
        auto it = m_map.find(m_data.at(position).name);
        if(it != m_map.end()) {
            m_map.erase(it);
        }
        m_data.removeAt(position);
    }

    endRemoveRows();
    return true;
}

//-----------------------------------------------------------------------------
void ConnectionModel::clear() {

    removeRows(0, rowCount());
}

//-----------------------------------------------------------------------------
void ConnectionModel::add(const Item &val) {

    auto it = m_map.find(val.name);
    if(it == m_map.end()) {
        beginInsertRows(QModelIndex(), m_data.size(), m_data.size());
        m_data.push_back(val);
        endInsertRows();
    }
    else {
        auto idx = index(it.value());
        emit dataChanged(idx, idx);
    }

    SQLHelper helper;
    QVector<QVariantList> vec;
    vec.push_back({val.name, val.host, val.user, val.pass});
    helper.insert(m_db, vec);
}

//-----------------------------------------------------------------------------
QHash<int, QByteArray> ConnectionModel::roleNames() const {

    auto roles = QAbstractListModel::roleNames();
    roles[ConnNameRole] = "connectionname";
    roles[HostRole] = "hostname";
    roles[UserRole] = "username";
    roles[PassRole] = "password";

    return roles;
}

//-----------------------------------------------------------------------------
void ConnectionModel::fill(const QVector<QVariantList> &vec) {

    if(vec.empty()) { return; }

    beginInsertRows(QModelIndex(), m_data.size(), m_data.size() + vec.size()-1);

    for(const QVariantList &lst : vec) {
        m_data.push_back({
            lst.at(0).toString(),
            lst.at(1).toString(),
            lst.at(2).toString(),
            lst.at(3).toString(),
        });
        m_map.insert(lst.at(0).toString(), m_data.size()-1);
    }

    endInsertRows();
}
