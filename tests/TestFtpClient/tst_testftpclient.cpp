#include <QString>
#include <QtTest>
#include <QCoreApplication>
#include "ftp.h"

class TestFtpClient : public QObject
{
    Q_OBJECT

public:
    TestFtpClient();

private:

    FtpStatus m_status;

private Q_SLOTS:
    void connectToHost_data();
    void connectToHost();

    void onFtpCommandFinished(FtpCommand c, FtpStatus status);
};

TestFtpClient::TestFtpClient()
{
}

void TestFtpClient::connectToHost_data() {

    QTest::addColumn<QString>("host");
    QTest::addColumn<uint>("port");
    QTest::addColumn<FtpStatus>("status");

    QTest::newRow( "ok01" ) << "127.0.0.1" << (uint)21 << FtpStatus::Ok;
    QTest::newRow( "error01" ) << "127.0.0.1" << (uint)2222 << FtpStatus::Error;
    QTest::newRow( "error02" ) << QString("foo.bar") << (uint)21 << FtpStatus::Error;
}

void TestFtpClient::connectToHost() {

    QFETCH( QString, host );
    QFETCH( uint, port );
    QFETCH( FtpStatus, status );

    Ftp ftp;
    connect(&ftp, &Ftp::commandFinished, this, &TestFtpClient::onFtpCommandFinished);

    ftp.connectToHost(host, port);

    QTest::qWait(10000);

    QCOMPARE( m_status, status );
}

void TestFtpClient::onFtpCommandFinished(FtpCommand c, FtpStatus status) {

    QTestEventLoop::instance().exitLoop();

    if(FtpCommand::ConnectToHost == c.type()) {
        m_status = status;
    }
}

QTEST_GUILESS_MAIN(TestFtpClient)

#include "tst_testftpclient.moc"
