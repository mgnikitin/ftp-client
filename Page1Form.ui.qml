import QtQuick 2.7
import QtQuick.Controls 2.0
import QtQuick.Layouts 1.3

Item {
    id: root
    property alias button1: button1
    property alias hostField: hostField
    property alias loginField: loginField
    property alias passField: passField
    property alias statusLabel: statusLabel
    property alias gridConnections: gridConnections
    property alias addButton: addButton

    ColumnLayout {
        id: columnLayout
        anchors.left: parent.left
        anchors.right: parent.right
        anchors.topMargin: 40
        anchors.top: parent.top
        anchors.bottom: parent.bottom

        GridLayout {
            anchors.horizontalCenter: parent.horizontalCenter
            columns: 2
            columnSpacing: 10

            Label {
                text: qsTr("host")
                Layout.alignment: Qt.AlignRight | Qt.AlignVCenter
                font.pointSize: 14
                fontSizeMode: Text.Fit
            }

            TextField {
                id: hostField
                placeholderText: qsTr("Enter host")
            }

            Label {
                text: qsTr("login")
                Layout.alignment: Qt.AlignRight | Qt.AlignVCenter
                font.pointSize: 14
                fontSizeMode: Text.Fit
            }

            TextField {
                id: loginField
                placeholderText: qsTr("Enter login")
            }

            Label {
                text: qsTr("password")
                Layout.alignment: Qt.AlignRight | Qt.AlignVCenter
                font.pointSize: 14
                fontSizeMode: Text.Fit
            }

            TextField {
                id: passField
                placeholderText: qsTr("Enter password")
                echoMode: TextInput.Password
            }
        }

        Row {
            id: buttonRow
            spacing: 10
            anchors.horizontalCenter: parent.horizontalCenter
            Button {
                id: button1
                text: qsTr("Connect")
            }
            Button {
                id: addButton
                text: qsTr("Add")
            }
        }
        GridView {
            id: gridConnections
            anchors.left: parent.left
            anchors.right: parent.right
            Layout.fillHeight: true
            anchors.margins: 5
            cellWidth: 110
            clip: true
        }
        Label {
            id: statusLabel
            anchors.horizontalCenter: parent.horizontalCenter
            anchors.bottom: parent.bottom
            anchors.margins: 5
        }
    }
}
