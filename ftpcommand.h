#ifndef FTPCOMMAND_H
#define FTPCOMMAND_H

#include <QStringList>

class FtpCommand {
public:

    enum Command {
        None,
        ConnectToHost,
        Login,
        Close,
        List,
        Cd,
        Get,
        Send
    };

//    Q_ENUMS(Command)

    // Empty command
    FtpCommand();

    Command type() const { return m_cmd; }

    QString toString() const;

    const QStringList &rawCommands() const { return m_raw; }
    QString param() const { return m_param; }

    static FtpCommand connectToHost(const QString &, quint16);
    static FtpCommand login(QString user, QString password);
    static FtpCommand close();
    static FtpCommand list(const QString &dir = QString());
    static FtpCommand cd(QString name);
    static FtpCommand get(QString name, bool passive, bool binary);
    static FtpCommand send(QString name, size_t size, bool passive, bool binary);

private:

    Command m_cmd;
    QStringList m_raw;
    QString m_param;

    FtpCommand(Command cmd_, const QStringList &r, const QString &param_ = QString());
};

#endif // FTPCOMMAND_H
