import QtQuick 2.7
import QtQuick.Controls 2.0
import QtQuick.Layouts 1.3
import "qrc:/qrc"

ApplicationWindow {
    visible: true
    width: 640
    height: 480
    title: qsTr("Ftp client")

    SwipeView {
        id: swipeView
        anchors.fill: parent

        Page1 {
        }

        LocalFilesPage {

        }

        RemoteFilesPage {

        }
    }

    footer: TabBar {
        id: tabBar
        currentIndex: swipeView.currentIndex
        TabButton {
            text: qsTr("connect")
        }
        TabButton {
            text: qsTr("local")
        }
        TabButton {
            text: qsTr("remote")
        }

        onCurrentIndexChanged: {
            swipeView.currentIndex = currentIndex
        }
    }

    Connections {
        target: Core

        onConnected: {
            if(val) {
                swipeView.currentIndex = 2
            }
        }
    }
}
