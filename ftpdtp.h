#ifndef FTPDTP_H
#define FTPDTP_H

#include "ftpconn.h"
#include "ftphelper.h"
#include "ftpstatus.h"

/** \brief Соедиение передачи данных
  */
class FtpDtp : public FtpConn {
    Q_OBJECT
public:

    enum State {
        Connected,
        Closed
    };

    FtpDtp();

    const QList<UrlInfo> &lastList() const { return m_list; }

    void setDevice(const QSharedPointer<QIODevice> &val);

    void setMode(FtpDtpMode val);

private:

    QList<UrlInfo> m_list;
    State m_state;
    FtpDtpMode m_mode;
    size_t m_fileSize;
    size_t m_bytesWritten;
    QSharedPointer<QIODevice> m_dev;

    void setState__(State s);

    void writePortion();

public slots:

    void onDtpFileSize(size_t val);
    void clearList();
    void onStartWriting();

private slots:

    void onReadyRead();
    void onError(QAbstractSocket::SocketError);
    void onSocketStateChanged(QAbstractSocket::SocketState);
    void onSocketBytesWritten(qint64 bytes);

signals:

    void listUpdated();
    void fileFinished(bool);
    void stateChanged(State);

};

QString stateToString(FtpDtp::State val);

#endif // FTPDTP_H
