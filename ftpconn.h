#ifndef FTPCONN_H
#define FTPCONN_H

#include <QTcpSocket>
#include <QHostAddress>

class FtpConn : public QObject {
    Q_OBJECT
public:
    FtpConn();

    // also include connecting state and host look up state
    bool isConnectedToHost();

    void connectToHost(const QString &host_, quint16 port_);

    QHostAddress host() const { return m_socket.peerAddress(); }
    quint16 port() const { return m_socket.peerPort(); }

public slots:

    void disconnectFromHost();

protected:

    QTcpSocket m_socket;
};

#endif // FTPCONN_H
