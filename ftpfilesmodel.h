#ifndef FTPFILESMODEL_H
#define FTPFILESMODEL_H

#include <QAbstractListModel>
#include <QUrl>
#include "urlinfo.h"
#include "ftp.h"

class FtpFilesModel : public QAbstractListModel
{
    Q_OBJECT

    Q_PROPERTY(QString folder READ folder WRITE setFolder NOTIFY folderChanged)
    Q_PROPERTY(QUrl parentFolder READ parentFolder NOTIFY parentFolderChanged)
public:

    enum Roles {
        FileNameRole = Qt::UserRole + 1,
        FileUrlRole,
        FileSizeRole
    };

    FtpFilesModel();

    virtual int rowCount(const QModelIndex &parent = QModelIndex()) const { if(parent.isValid()) return 0; return m_data.size(); }

    virtual QVariant data(const QModelIndex &index, int role) const;

    virtual QHash<int, QByteArray> roleNames() const;

    virtual bool removeRows(int position, int rows, const QModelIndex &parent = QModelIndex());

    QString folder() const { return m_folder.toString(); }
    void setFolder(QString val);

    QUrl parentFolder() const;

    Q_INVOKABLE bool isFolder(int index) const;

    Q_INVOKABLE QVariant get(int idx, const QString &property) const;

private:

    QList<UrlInfo> m_data;
    QUrl m_folder;

    void clear();
    void setFolder__(QString val);

private slots:

    void onListChanged();
    void onFtpCommandFinished(FtpCommand c, FtpStatus );

signals:

    void folderChanged();
    void parentFolderChanged();
};

#endif // FTPFILESMODEL_H
