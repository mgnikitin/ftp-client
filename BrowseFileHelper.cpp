#include "BrowseFileHelper.h"
#include <QStandardPaths>
#include <QUrl>
#include <QDir>
#include <QStorageInfo>
#include <QLoggingCategory>

Q_LOGGING_CATEGORY(browseFileHelper, "browseFileHelper", QtDebugMsg)

BrowseFileHelper::BrowseFileHelper() {

}

QString BrowseFileHelper::startPath() const {

    QString path = QStandardPaths::standardLocations(QStandardPaths::HomeLocation).first();
    QString url = QUrl::fromLocalFile(path).toString();
    return url; // QDir::toNativeSeparators(url);
}

QString BrowseFileHelper::localStringToUrl(QString str) const {

    return QUrl::fromLocalFile(str).toString();
}

QString BrowseFileHelper::urlToString(QString url) const {

    QUrl u(url);
    return u.toLocalFile();
}

QString BrowseFileHelper::pathToUrl(QString text) const {

    QString prefix;
    if('/' != QDir::separator()) {
        text = text.replace('/', QDir::separator());
    }
    if(text.startsWith(QDir::separator())) { prefix = QDir::separator(); }
    QStringList items = text.split(QDir::separator(), QString::SkipEmptyParts);
    QString path;
    for(auto item : items) {
        QString ph = !path.isEmpty() ? (path + QDir::separator()) : QString();
        ph += item;
        QFileInfo fi(prefix + ph);
        if(!fi.exists() || !fi.isDir()) {
            break;
        }
        if(path.isEmpty()) { path += item; }
        else { path += QDir::separator() + item; }
    }

    QFileInfo fi(prefix + path);
    if(fi.isDir()) {
        return localStringToUrl(fi.absoluteFilePath() + QDir::separator());
    }
    return localStringToUrl(fi.absoluteFilePath());
}


