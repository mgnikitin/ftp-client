#include <QGuiApplication>
#include <QQmlApplicationEngine>
#include <QQmlContext>
#include "core.h"
#include "BrowseFileHelper.h"
#include "ftpfilesmodel.h"
#include "connectionmodel.h"

int main(int argc, char *argv[])
{
    qSetMessagePattern("%{time [hh:mm:ss.zzz]} %{type} %{category}: %{message} (%{function})");
    QCoreApplication::setAttribute(Qt::AA_EnableHighDpiScaling);
    QGuiApplication app(argc, argv);

    qmlRegisterType<BrowseFileHelper>("BrowseFileHelper", 1, 0, "BrowseFileHelper");
    qmlRegisterType<FtpFilesModel>("FtpFilesModel", 1, 0, "FtpFilesModel");
    qmlRegisterType<ConnectionModel>("ConnectionModel", 1, 0, "ConnectionModel");

    QQmlApplicationEngine engine;
    engine.rootContext()->setContextProperty("Core", static_cast<QObject *>(&Core::instance()));
    engine.load(QUrl(QLatin1String("qrc:/main.qml")));
    if (engine.rootObjects().isEmpty())
        return -1;

    return app.exec();
}
