#include "ftpfilesmodel.h"
#include "core.h"

//-----------------------------------------------------------------------------
FtpFilesModel::FtpFilesModel()
{
    m_folder.setScheme("file");
    connect(&Core::instance().ftp(), &Ftp::commandFinished,
            this, &FtpFilesModel::onFtpCommandFinished);
    connect(&Core::instance().ftp(), &Ftp::listChanged,
            this, &FtpFilesModel::onListChanged);
}

//-----------------------------------------------------------------------------
QVariant FtpFilesModel::data(const QModelIndex &index, int role) const {

    if(!index.isValid())
        return QVariant();

    if((index.row() < 0) || (index.row() >= m_data.size()))
        return QVariant();

    switch(role) {
    case Qt::DisplayRole:
    case FileNameRole:
        return m_data.at(index.row()).toString();
    case FileUrlRole: {
        QUrl url = m_folder;
        url.setPath(url.path() + "/" + m_data.at(index.row()).toString());
        return url.toString();
    }
    case FileSizeRole:
        return m_data.at(index.row()).size();
    default:
        return QVariant();
    }
}

//-----------------------------------------------------------------------------
QHash<int, QByteArray> FtpFilesModel::roleNames() const {

    auto roles = QAbstractListModel::roleNames();
    roles[FileNameRole] = "fileName";
    roles[FileUrlRole] = "fileURL";
    roles[FileSizeRole] = "fileSize";

    return roles;
}

//-----------------------------------------------------------------------------
bool FtpFilesModel::removeRows(int position, int rows, const QModelIndex &parent) {

    if((rows < 1) || (position < 0) || ((position + rows) > m_data.size()))
        return false;
    beginRemoveRows(parent, position, qMax(0, position+rows-1));

    for (int row = 0; row < rows; ++row) {
        m_data.removeAt(position);
    }

    endRemoveRows();
    return true;
}

//-----------------------------------------------------------------------------
void FtpFilesModel::clear() {

    removeRows(0, rowCount());
}

//-----------------------------------------------------------------------------
void FtpFilesModel::setFolder(QString val) {

    QUrl url = val;
    if(m_folder != url) {
        if(!url.path().isEmpty()) {
            Core::instance().ftp().command(FtpCommand::cd(url.path()));
            Core::instance().ftp().command(FtpCommand::list());
        }
        else {
            setFolder__(val);
        }
    }
}

//-----------------------------------------------------------------------------
void FtpFilesModel::setFolder__(QString val) {

    QUrl url = m_folder;
    url.setPath(val);
    if(m_folder != url) {
        qDebug() << QString("folder %1").arg(url.toString());
        m_folder = url;
        emit folderChanged();
        emit parentFolderChanged();
    }
}

//-----------------------------------------------------------------------------
QUrl FtpFilesModel::parentFolder() const {

    QUrl url = m_folder;
    url.setPath(url.path().left(url.path().lastIndexOf('/') + 1));
    return url;
}

//-----------------------------------------------------------------------------
bool FtpFilesModel::isFolder(int index) const {

    if(index < 0) return false;
    return m_data.at(index).isDir();
}

//-----------------------------------------------------------------------------
QVariant FtpFilesModel::get(int idx, const QString &property) const {

    int role = roleNames().key(property.toLatin1(), -1);
    if (role >= 0 && idx >= 0)
        return data(index(idx, 0), role);
    else
        return QVariant();
}

//-----------------------------------------------------------------------------
void FtpFilesModel::onListChanged() {

    clear();

    const auto &files = Core::instance().ftp().files();
    if(files.size() != 0) {
        beginInsertRows(QModelIndex(), m_data.size(), m_data.size() + files.size()-1);
        for(auto &item : files) {
            m_data.append(item);
        }
        endInsertRows();
    }

    if(m_folder.path().isEmpty()) {
//        setFolder("file:///");
        m_folder = "file:///";
        emit folderChanged();
    }
}

// ----------------------------------------------------------------------------
void FtpFilesModel::onFtpCommandFinished(FtpCommand c, FtpStatus status) {

    if(FtpCommand::Login == c.type()) {
        if(FtpStatus::Ok == status) {
            Core::instance().ftp().command(FtpCommand::list());
        }
        setFolder__("");
    }
    else if(FtpCommand::ConnectToHost == c.type()) {
        setFolder__("");
    }
    else if(FtpCommand::Cd == c.type()) {
        if(FtpStatus::Ok == status) {
            setFolder__(c.param());
        }
    }
//    else if(FtpCommand::Get == c.type()) {

//    }
}
