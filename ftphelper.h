#ifndef FTPHELPER_H
#define FTPHELPER_H

#include "urlinfo.h"

class FtpHelper
{
public:
    FtpHelper();

    bool parseUnixDir(const QStringList &tokens, UrlInfo &info);

    bool parseDir(QString line, UrlInfo &info);
};

#endif // FTPHELPER_H
