#ifndef SQLHELPER_H
#define SQLHELPER_H

#include <QSqlDatabase>
#include <QVariantList>

class SQLHelper
{
public:
    SQLHelper();

    bool createTableIfNotExists(QSqlDatabase &db_);
    bool createIndexIfNotExists(QSqlDatabase &db_, QString column, QString indexName_ = QString());

    bool insert(QSqlDatabase &db_, const QVector<QVariantList> &vec);
    bool read(QSqlDatabase &db_, QVector<QVariantList> &vec);

    const QString TABLE_NAME = "connections";
    const QString COLUMN_CONN_ID = "id";
    const QString COLUMN_CONN_NAME = "name";
    const QString COLUMN_CONN_HOST = "host";
    const QString COLUMN_CONN_USER = "user";
    const QString COLUMN_CONN_PASS = "pass";

private:
};

#endif // SQLHELPER_H
