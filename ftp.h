#ifndef FTP_H
#define FTP_H

#include <QObject>
#include <QHostAddress>
#include <QSslError>
#include "ftppi.h"
#include "ftpcommand.h"

class Ftp : public QObject
{
    Q_OBJECT

    Q_PROPERTY(QStringList list READ list NOTIFY listChanged)

public:

    enum TransferType {
        Binary,
        Ascii
    };

    explicit Ftp(QObject *parent = nullptr);

    void login(QString user, QString password);

    const QList<UrlInfo> &files() const;
    QStringList list() const;

    void get(const QString &file, const QSharedPointer<QIODevice> &dev, TransferType type);

    void send(const QString &file, const QSharedPointer<QIODevice> &dev, TransferType type);

    void command(const FtpCommand &c);

    bool connectToHost(const QString &host, quint16 port=21);

    bool isQueued() const { return !m_queue.empty(); }

    void clear();

private:

    /// \brief Очередь команд
    QList<FtpCommand> m_queue;
    FtpPi m_pi;
    FtpDtp m_dtp;

    void addCommand(const FtpCommand &cmd_);
    void startNextCommand();

public slots:

private slots:

    void onSslErrors(const QList<QSslError> &lst);
    void onPiFinished(FtpStatus);

    void onDtpConnect(QString host, quint16 port);

signals:

    void commandFinished(FtpCommand, FtpStatus);
    void listChanged();
};

#endif // FTP_H
