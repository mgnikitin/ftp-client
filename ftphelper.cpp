#include "ftphelper.h"
#include <QLoggingCategory>

Q_LOGGING_CATEGORY(ftpHelper, "ftpHelper", QtWarningMsg)

// ----------------------------------------------------------------------------
FtpHelper::FtpHelper()
{

}

// ----------------------------------------------------------------------------
bool FtpHelper::parseUnixDir(const QStringList &tokens, UrlInfo &info) {

    if (tokens.size() != 8) {
        return false;
    }

    // element type
    char first = tokens.at(1).at(0).toLatin1();
    if ('d' == first) {
        info.setType(UrlInfo::Dir);
    }
    else if('-' == first) {
        info.setType(UrlInfo::File);
    }
    else if('l' == first) {
        info.setType(UrlInfo::Symlink);
    }
    else {
        qCWarning(ftpHelper) << QString("Couldn't decode element type: %1").arg(first);
        return false;
    }

    // Resolve filename
    QString name = tokens.at(7);
    if (info.isSymLink()) {
        int linkPos = name.indexOf(QLatin1String(" ->"));
        if (linkPos != -1)
            name.resize(linkPos);
    }
    info.setName(name);

    // Resolve owner & group
    info.setOwner(tokens.at(3));
    info.setGroup(tokens.at(4));

    // Resolve size
    info.setSize(tokens.at(5).toLongLong());

    return true;
}

// ----------------------------------------------------------------------------
bool FtpHelper::parseDir(QString line, UrlInfo &info) {

    QRegExp unixPattern(QLatin1String("^([\\-dl])([a-zA-Z\\-]{9,9})\\s+\\d+\\s+(\\S*)\\s+"
                                      "(\\S*)\\s+(\\d+)\\s+(\\S+\\s+\\S+\\s+\\S+)\\s+(\\S.*)"));

    if (unixPattern.indexIn(line.trimmed()) == 0) {
        return parseUnixDir(unixPattern.capturedTexts(), info);
    }

    qCWarning(ftpHelper) << QString("Couldn't parse line: %1").arg(line);
    return false;
}

