#include "ftp.h"
#include <QLoggingCategory>

Q_LOGGING_CATEGORY(ftp, "ftp", QtDebugMsg)

// ----------------------------------------------------------------------------
Ftp::Ftp(QObject *parent) : QObject(parent)
{
    connect(&m_pi, &FtpPi::finished, this, &Ftp::onPiFinished);
    connect(&m_pi, &FtpPi::dtpConnect, this, &Ftp::onDtpConnect);
    connect(&m_pi, &FtpPi::dtpDisconnect, &m_dtp, &FtpConn::disconnectFromHost);
    connect(&m_pi, &FtpPi::dtpWrite, &m_dtp, &FtpDtp::onStartWriting);
    connect(&m_pi, &FtpPi::dtpMode, &m_dtp, &FtpDtp::setMode);
    connect(&m_pi, &FtpPi::dtpClearList, &m_dtp, &FtpDtp::clearList);
    connect(&m_pi, &FtpPi::dtpFileSize, &m_dtp, &FtpDtp::onDtpFileSize);
    connect(&m_dtp, &FtpDtp::listUpdated, this, &Ftp::listChanged);
    connect(&m_dtp, &FtpDtp::fileFinished, &m_pi, &FtpPi::onDtpFinished);
    connect(&m_dtp, &FtpDtp::stateChanged, &m_pi, &FtpPi::onDtpState);
}

// ----------------------------------------------------------------------------
void Ftp::login(QString user, QString password) {

    addCommand(FtpCommand::login(user, password));
}

// ----------------------------------------------------------------------------
const QList<UrlInfo> &Ftp::files() const {

    return m_dtp.lastList();
}

// ----------------------------------------------------------------------------
QStringList Ftp::list() const {

    QStringList items;
    const auto &list = m_dtp.lastList();
    for(const UrlInfo &item : list) {
        items << item.toString();
    }

    return items;
}

// ----------------------------------------------------------------------------
void Ftp::get(const QString &file, const QSharedPointer<QIODevice> &dev, TransferType type) {

    m_dtp.setDevice(dev);
    addCommand(FtpCommand::get(file, true, type == Binary));
}

// ----------------------------------------------------------------------------
void Ftp::send(const QString &file, const QSharedPointer<QIODevice> &dev, TransferType type) {

    m_dtp.setDevice(dev);
    qint64 sz = 0;
    if(dev) {
        sz = dev->size();
    }
    addCommand(FtpCommand::send(file, sz, true, type == Binary));
}

// ----------------------------------------------------------------------------
void Ftp::command(const FtpCommand &c) {

    addCommand(c);
}

// ----------------------------------------------------------------------------
bool Ftp::connectToHost(const QString &host, quint16 port) {

    qCDebug(ftp) << QString("connecting to host %1:%2").arg(host).arg(port);
    if(m_dtp.isConnectedToHost()) {
        m_dtp.disconnectFromHost();
    }
    m_pi.connectToHost(host, port);
    addCommand(FtpCommand::connectToHost(host, port));

    return true;
}

// ----------------------------------------------------------------------------
void Ftp::clear() {

    m_pi.abort();
    // очищаем очередь команд
    m_queue.clear();
}

// ----------------------------------------------------------------------------
void Ftp::addCommand(const FtpCommand &cmd_) {

    m_queue.append(cmd_);
    if(1 == m_queue.size()) {
        startNextCommand();
    }
}

// ----------------------------------------------------------------------------
void Ftp::startNextCommand() {

    if(m_queue.empty()) { return; }

    auto cmd_ = m_queue.first();
    m_pi.sendCommand(cmd_);
}

// ----------------------------------------------------------------------------
void Ftp::onSslErrors(const QList<QSslError> &lst) {

    QString str;
    for(QSslError error : lst) {
        str.append(QString("%1").arg(error.errorString()));
    }
    qCWarning(ftp) << QString("ssl errors: %1").arg(str);
}

// ----------------------------------------------------------------------------
void Ftp::onPiFinished(FtpStatus s) {

    emit commandFinished(m_pi.lastCommand(), s);

    // убираем команду из очереди
    if(!m_queue.empty()) {
        m_queue.pop_front();
    }
    startNextCommand();
}

// ----------------------------------------------------------------------------
void Ftp::onDtpConnect(QString host, quint16 port) {

    if(!m_dtp.isConnectedToHost() || m_dtp.host() != QHostAddress(host) || m_dtp.port() != port) {
        m_dtp.connectToHost(host, port);
    }
}
