#include "ftpconn.h"
#include <QLoggingCategory>

Q_LOGGING_CATEGORY(ftpConn, "ftpConn", QtDebugMsg)

// ----------------------------------------------------------------------------
FtpConn::FtpConn()
{

}

// ----------------------------------------------------------------------------
bool FtpConn::isConnectedToHost() {

    auto st = m_socket.state();
    return QAbstractSocket::HostLookupState == st
            || QAbstractSocket::ConnectingState == st
            || QAbstractSocket::ConnectedState == st;
}

// ----------------------------------------------------------------------------
void FtpConn::connectToHost(const QString& host_, quint16 port_) {

    if(QAbstractSocket::UnconnectedState != m_socket.state()){
        disconnectFromHost();
    }
    qCDebug(ftpConn) << QString("%1 connecting to host %2:%3")
                        .arg(objectName()).arg(host_).arg(port_);
    m_socket.connectToHost(host_, port_);
}

// ----------------------------------------------------------------------------
void FtpConn::disconnectFromHost() {

    qCDebug(ftpConn) << QString("%1 disconnectFromHost").arg(objectName());

    if(QAbstractSocket::ClosingState != m_socket.state()) {
        m_socket.disconnectFromHost();
    }
    if(QAbstractSocket::UnconnectedState != m_socket.state()) {
        m_socket.waitForDisconnected();
    }
}
