import QtQuick 2.7
import QtQuick.Controls 2.2
import ConnectionModel 1.0

Page1Form {
    button1.onClicked: {
        Core.connectToHost()
    }

    addButton.onClicked: {
        Core.addConnection()
    }

    hostField.text: Core.host

    hostField.onTextChanged: {
        Core.host = hostField.text
    }

    loginField.text: Core.user

    loginField.onTextChanged: {
        Core.user = loginField.text
    }

    passField.text: Core.password
    passField.onTextChanged: {
        Core.password = passField.text
    }

    Connections {
        target: passField
        onAccepted: {
            if(passField.focus) {
                Core.connectToHost()
            }
        }
    }

    statusLabel.text: Core.status

    gridConnections.model: Core.connectionModel

    gridConnections.onCurrentIndexChanged: {
        chooseItem()
    }

    gridConnections.delegate: Text {
        id: itemText
        text: model.connectionname

        MouseArea {
            id: mouseArea
            anchors.fill: parent

            onClicked: {
                gridConnections.currentIndex = model.index
            }
        }
    }


    function chooseItem() {
        var idx = gridConnections.model.index(gridConnections.currentIndex, 0)
        hostField.text = gridConnections.model.data(idx, ConnectionModel.HostRole)
        loginField.text = gridConnections.model.data(idx, ConnectionModel.UserRole)
        passField.text = gridConnections.model.data(idx, ConnectionModel.PassRole)
    }

}
