#include "ftppi.h"
#include <QLoggingCategory>

Q_LOGGING_CATEGORY(ftpPi, "ftpPi", QtDebugMsg)

// ----------------------------------------------------------------------------
FtpPi::FtpPi()
    : m_waitFlags(0)
    , m_lastDtpState(FtpDtp::Closed)
    , m_finished(true)
    , m_state(Idle)
{
    setObjectName("FtpPi");
    qRegisterMetaType<QAbstractSocket::SocketError>("QAbstractSocket::SocketError");
    connect(&m_socket, &QIODevice::readyRead, this, &FtpPi::onReadyRead);
    connect(&m_socket, static_cast<void (QAbstractSocket::*)(QAbstractSocket::SocketError)>(&QAbstractSocket::error),
            this, &FtpPi::onError);
}

// ----------------------------------------------------------------------------
void FtpPi::abort() {

    qCDebug(ftpPi) << QString("abort");
    m_cmds.clear();
    m_socket.write("ABOR\r\n");
}

// ----------------------------------------------------------------------------
bool FtpPi::sendCommand(const FtpCommand &cmd_) {

    if(!m_socket.isOpen()) {
        qCWarning(ftpPi) << QString("not opened");
        return false;
    }

    m_lastCmd = cmd_;
    m_cmds = m_lastCmd.rawCommands();
    m_finished = false;

    //
    if(FtpCommand::List == m_lastCmd.type()) {
        emit dtpMode(FtpDtpMode::List);
        emit dtpClearList();
    }
    else if(FtpCommand::ConnectToHost == m_lastCmd.type()) {
        m_waitFlags |= WaitConnect;
    }
    else if(FtpCommand::Get == m_lastCmd.type()) {
        emit dtpMode(FtpDtpMode::FileRead);
    }
    // in case FtpCommand::Send FtpDtpMode::FileWrite is called later

    nextCommand();

    return true;
}

// ----------------------------------------------------------------------------
void FtpPi::nextCommand() {

    if(m_waitFlags != 0) {
      return; // waitining
    }

    if(m_cmds.empty()) {
        m_finished = true;
        emit finished(FtpStatus::Ok);
        return;
    }

    QString cmd = m_cmds.takeFirst();

    qCDebug(ftpPi) << QString("send: %1").arg(cmd);
    m_socket.write(cmd.toLatin1());
    m_waitFlags |= WaitReply;
}

// ----------------------------------------------------------------------------
void FtpPi::clear__(FtpStatus s) {

    m_cmds.clear();
    m_finished = true;
    m_waitFlags = 0;
    emit dtpMode(FtpDtpMode::None);
    emit finished(s);
}

// ----------------------------------------------------------------------------
bool FtpPi::decodeAddress(QString line, QString &host, quint16 &port) const {

    QRegExp addrPortPattern(QLatin1String("(\\d+),(\\d+),(\\d+),(\\d+),(\\d+),(\\d+)"));
    if (addrPortPattern.indexIn(line) == -1) {
        qCWarning(ftpPi) << QString("Couldn't decode address %1").arg(line);
        return false;
    }

    QStringList lst = addrPortPattern.capturedTexts();
    host = lst[1] + QLatin1Char('.') + lst[2] + QLatin1Char('.') + lst[3] + QLatin1Char('.') + lst[4];
    port = (lst[5].toUInt() << 8) + lst[6].toUInt();
    return true;
}

// ----------------------------------------------------------------------------
bool FtpPi::decode(const QString &line) {

    const int replyCodeInt = 100*m_replyCode[0] + 10*m_replyCode[1] + m_replyCode[2];
    m_state = static_cast<State>(m_replyCode[0]);
    qCDebug(ftpPi) << QString("reply %1").arg(replyCodeInt);

    switch (replyCodeInt) {
    case 125:   // Data connection already open; transfer starting.
    case 150:   // File status okay; about to open data connection
        if(FtpCommand::Send == m_lastCmd.type()) {
            if(FtpDtp::Connected == m_lastDtpState) { // already connected
                emit dtpWrite(); // write data
            }
            else {
                m_waitFlags |= WaitDtpTask; // start write later
            }
        }
        break;
    case 200:   // Command okay
    case 202:   // Command not implemented, superfluous at this site
        break;
    case 213:   // File status.
        if(FtpCommand::Get == m_lastCmd.type()) {
            size_t sz = m_replyText.simplified().toLongLong();
            emit dtpFileSize(sz);
        }
        break;
    case 220:   // Service ready for new user
        m_waitFlags &= ~WaitConnect;
        break;
    case 226:   // Closing data connection.
        if(FtpDtp::Closed != m_lastDtpState) {
            m_waitFlags |= WaitDtpDisconnect;
        }
//        emit dtpDisconnect();
        break;
    case 227: {  // Entering Passive Mode
        QString host;
        quint16 port;
        if(decodeAddress(line, host, port)) {
            m_waitFlags |= WaitDtpConnect;
            emit dtpConnect(host, port);
        }
        else {
            clear__(FtpStatus::Error);
        }
        break;
    }
    case 230:   // User logged in, proceed
        if(!m_cmds.empty() && m_cmds.first().startsWith(QString("PASS "))) {
            m_cmds.pop_front();
        }
        break;
    case 250:   // Requested file action okay, completed.
        break;
    case 331:   // User name okay, need password.
        break;
    case 421:   // Service not available, closing control connection
        emit dtpDisconnect();
        disconnectFromHost();
        break;
    case 500:   // Syntax error, command unrecognized
        break;
    case 550:   // Requested action not taken.
                // File unavailable (e.g., file not found, no access).
    case 553:   // Requested action not taken. File name not allowed.
        clear__(FtpStatus::FileUnavailable);
        emit dtpDisconnect();
        break;
    case 530:   // Login incorrect
        clear__(FtpStatus::LoginIncorrect);
        break;
    default:
        return false;
    }

    return true;
}

// ----------------------------------------------------------------------------
bool FtpPi::verifyCode(const QString &line) {

    if(line.size() < 3) {
        qCWarning(ftpPi) << QString("Couldn't decode line: %1").arg(line);
        return false;
    }

    const int lowerLimit[3] = {1,0,0};
    const int upperLimit[3] = {5,5,9};

    for (int i=0; i<3; i++) {
        m_replyCode[i] = line.at(i).digitValue();
        if (m_replyCode[i]<lowerLimit[i] || m_replyCode[i]>upperLimit[i]) {
            qCWarning(ftpPi) << QString("Couldn't decode line: %1").arg(line);
            return false;
        }
    }

    return true;
}

// ----------------------------------------------------------------------------
void FtpPi::onReadyRead() {

    while (m_socket.canReadLine()) {
        QString line = m_socket.readLine();
        qCDebug(ftpPi) << QString("read line: %1").arg(line);
        if(m_replyText.isEmpty()) {
            if(!verifyCode(line)) {
                continue;
            }
        }

        QString endOfMultiLine;
        endOfMultiLine[0] = '0' + m_replyCode[0];
        endOfMultiLine[1] = '0' + m_replyCode[1];
        endOfMultiLine[2] = '0' + m_replyCode[2];
        endOfMultiLine[3] = QLatin1Char(' ');
        QString lineCont(endOfMultiLine);
        lineCont[3] = QLatin1Char('-');
        QString lineLeft4 = line.left(4);

        while (lineLeft4 != endOfMultiLine) {
            if (lineLeft4 == lineCont)
                m_replyText += line.mid(4); // strip 'xyz-'
            else
                m_replyText += line;
            if (!m_socket.canReadLine())
                return;
            line = m_socket.readLine();
            lineLeft4 = line.left(4);
        }
        m_replyText += line.mid(4); // strip reply code 'xyz '
        if (m_replyText.endsWith(QLatin1String("\r\n")))
            m_replyText.chop(2);

        m_waitFlags &= ~WaitReply;

        if(!decode(line)) {
            qCWarning(ftpPi) << QString("Couldn't decode line: %1").arg(line);
            clear__(FtpStatus::Error);
            emit dtpDisconnect();
            m_replyText = QString();
        }
        else {
            m_replyText = QString();
        }
    }

    if(!m_finished && m_state != Waiting) {
        nextCommand();
    }
}

// ----------------------------------------------------------------------------
void FtpPi::onError(QAbstractSocket::SocketError error_) {

    qCWarning(ftpPi) << QString("error %1").arg(error_);
    clear__(FtpStatus::Error);
}

// ----------------------------------------------------------------------------
void FtpPi::onDtpState(FtpDtp::State s) {

    m_lastDtpState = s;

    switch (s) {
    case FtpDtp::Connected:
        if(m_waitFlags & WaitDtpTask) {
            m_waitFlags &= ~WaitDtpConnect;
            emit dtpWrite();  // write data
        }
        else if(m_waitFlags & WaitDtpConnect) {
            m_waitFlags &= ~WaitDtpConnect;
            if(!m_finished) {
                nextCommand();
            }
        }

        break;
    case FtpDtp::Closed:
        m_waitFlags &= ~WaitDtpTask;

//        if(m_waitFlags & WaitDtpDisconnect) {
            m_waitFlags &= ~WaitDtpDisconnect;
            if(!m_finished) {
                nextCommand();
            }
//        }

        break;
    default: // error
        clear__(FtpStatus::Error);
        break;
    }
}

// ----------------------------------------------------------------------------
void FtpPi::onDtpFinished(bool success) {

    m_waitFlags &= ~WaitDtpTask;
    if(!success) {
        clear__(FtpStatus::Error);
    }
    emit dtpDisconnect();
}
