#ifndef CONFIG_H
#define CONFIG_H

#include <QString>

class Config
{
public:

    static Config &instance();

    QString dataPath() const;

    QString databaseFile() const { return m_dbFile; }
    void setDatabaseFile(QString val);

private:

    QString m_dbFile;

    Config();

    Config(const Config &) = delete;
    Config &operator =(const Config &) = delete;
};

#endif // CONFIG_H
