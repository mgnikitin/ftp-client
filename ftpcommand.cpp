#include "ftpcommand.h"

// ----------------------------------------------------------------------------
FtpCommand::FtpCommand()
    : m_cmd(None)
{

}

// ----------------------------------------------------------------------------
FtpCommand::FtpCommand(Command cmd_, const QStringList &r, const QString &param_)
    : m_cmd(cmd_), m_raw(r), m_param(param_)
{

}

// ----------------------------------------------------------------------------
QString FtpCommand::toString() const {

    switch (m_cmd) {
    case None: return QString::fromUtf8("None");
    case ConnectToHost: return QString::fromUtf8("ConnectToHost");
    case Login: return QString::fromUtf8("Login");
    case Close: return QString::fromUtf8("Close");
    case List: return QString::fromUtf8("List");
    case Cd: return QString::fromUtf8("Cd");
    case Get: return QString::fromUtf8("Get");
    case Send: return QString::fromUtf8("Send");
    }

    // не должны сюда попасть
    return QString::fromUtf8("[undefined]");
}

// ----------------------------------------------------------------------------
FtpCommand FtpCommand::connectToHost(const QString &, quint16 ) {

    QStringList raw;

    return FtpCommand(ConnectToHost, raw);
}

// ----------------------------------------------------------------------------
FtpCommand FtpCommand::login(QString user, QString password) {

    QStringList raw;

    if(user.isEmpty()) {
        user = QString::fromUtf8("anonymous");
        if(password.isEmpty()) {
            password = QString::fromUtf8("anonymous");
        }
    }

    raw.append(QString("USER %1\r\n").arg(user));
    raw.append(QString("PASS %1\r\n").arg(password));

    return FtpCommand(Login, raw);
}

// ----------------------------------------------------------------------------
FtpCommand FtpCommand::close() {

    QStringList raw;

    raw.append(QString("QUIT\r\n"));

    return FtpCommand(Close, raw);
}

// ----------------------------------------------------------------------------
FtpCommand FtpCommand::list(const QString &dir) {

    QStringList raw;

    raw.append(QString("TYPE A\r\n"));
    raw.append(QString("PASV\r\n"));
    if (dir.isEmpty())
        raw.append(QString("LIST\r\n"));
    else
        raw.append(QString("LIST ") + dir + QString("\r\n"));


    return FtpCommand(List, raw);
}

// ----------------------------------------------------------------------------
FtpCommand FtpCommand::cd(QString name) {

    QStringList raw;

    raw.append(QString("CWD %1\r\n").arg(name));

    return FtpCommand(Cd, raw, name);
}

// ----------------------------------------------------------------------------
FtpCommand FtpCommand::get(QString name, bool passive, bool binary) {

    QStringList raw;

    if (binary)
        raw << QString("TYPE I\r\n");
    else
        raw << QString("TYPE A\r\n");
    raw << QString("SIZE ") + name + QString("\r\n"); // TODO only for files
    raw << QString(passive ? "PASV\r\n" : "PORT\r\n");
    raw << QString("RETR ") + name + QString("\r\n");

    return FtpCommand(Get, raw, name);
}

// ----------------------------------------------------------------------------
FtpCommand FtpCommand::send(QString name, size_t size, bool passive, bool binary) {

    QStringList raw;

    if (binary)
        raw << QString("TYPE I\r\n");
    else
        raw << QString("TYPE A\r\n");
    raw << QString(passive ? "PASV\r\n" : "PORT\r\n");
    raw << QLatin1String("ALLO ") + QString::number(size) + QString("\r\n");
    raw << QLatin1String("STOR ") + name + QLatin1String("\r\n");

    return FtpCommand(Send, raw, name);
}
