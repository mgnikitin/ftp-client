#include "sqlhelper.h"
#include <QSqlQuery>
#include <QSqlRecord>
#include <QSqlError>
#include <QDebug>

SQLHelper::SQLHelper()
{

}

bool SQLHelper::createTableIfNotExists(QSqlDatabase &db_) {

    QString str;
    str.append(QString("CREATE TABLE IF NOT EXISTS %1 (").arg(TABLE_NAME));
    str.append(QString("%1 INTEGER PRIMARY KEY").arg(COLUMN_CONN_ID));
    str.append(QString(", %1 TEXT").arg(COLUMN_CONN_NAME));
    str.append(QString(", %1 TEXT").arg(COLUMN_CONN_HOST));
    str.append(QString(", %1 TEXT").arg(COLUMN_CONN_USER));
    str.append(QString(", %1 TEXT").arg(COLUMN_CONN_PASS));
    str.append(");");

    db_.exec(str);
    if(db_.lastError().isValid()) {
        qWarning() << db_.lastError();
        return false;
    }

    return true;
}

bool SQLHelper::createIndexIfNotExists(QSqlDatabase &db_, QString column, QString indexName_) {

    if(indexName_.isEmpty()) {
        indexName_.append(QString("idx_%1_%2").arg(TABLE_NAME).arg(column));
    }
    QString str;
    str.append(QString("CREATE UNIQUE INDEX IF NOT EXISTS %1 ON %2 (%3);")
               .arg(indexName_).arg(TABLE_NAME).arg(column));

    db_.exec(str);
    if(db_.lastError().isValid()) {
        qWarning() << db_.lastError();
        return false;
    }

    return true;
}

bool SQLHelper::insert(QSqlDatabase &db_, const QVector<QVariantList> &vec) {

    if(vec.empty()) { return true; }

    QString str;
    str.append(QString("INSERT OR REPLACE INTO %1 (").arg(TABLE_NAME));
    str.append(QString("%1").arg(COLUMN_CONN_NAME));
    str.append(QString(", %1").arg(COLUMN_CONN_HOST));
    str.append(QString(", %1").arg(COLUMN_CONN_USER));
    str.append(QString(", %1").arg(COLUMN_CONN_PASS));
    str.append(") VALUES ");
    for(auto i = 0; i < vec.size(); ++i) {
        const QVariantList &lst = vec.at(i);
        if(0 == i) {
            str.append("(");
        }
        else {
            str.append(", (");
        }

        str.append(QString("\"%1\"").arg(lst.at(0).toString()));
        str.append(QString(",\"%1\"").arg(lst.at(1).toString()));
        str.append(QString(",\"%1\"").arg(lst.at(2).toString()));
        str.append(QString(",\"%1\"").arg(lst.at(3).toString()));
        str.append(")");
    }
    str.append(";");

    db_.exec(str);

    if(db_.lastError().isValid()) {
        qWarning() << db_.lastError();
        return false;
    }
    return true;
}

bool SQLHelper::read(QSqlDatabase &db_, QVector<QVariantList> &vec) {

    QSqlQuery query = db_.exec(QString("SELECT %1, %2, %3, %4 FROM %5;")
                              .arg(COLUMN_CONN_NAME)
                              .arg(COLUMN_CONN_HOST)
                              .arg(COLUMN_CONN_USER)
                              .arg(COLUMN_CONN_PASS)
                              .arg(TABLE_NAME));

    if(db_.lastError().isValid()) {
        qWarning() << db_.lastError();
        return false;
    }

    while (query.next()) {
        QSqlRecord record = query.record();
        QVariantList lst;
        lst.push_back(record.value(0));
        lst.push_back(record.value(1));
        lst.push_back(record.value(2));
        lst.push_back(record.value(3));
        vec.append(lst);
    }

    return true;
}

