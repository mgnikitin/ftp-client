#include "config.h"
#include <QCoreApplication>
#include <QStandardPaths>
#include <QDir>
#ifdef __ANDROID__
#include <QAndroidJniEnvironment>
#include <QAndroidJniObject>
#endif

Config::Config()
{
    QDir dir;
    dir.mkpath(dataPath());
    m_dbFile = dataPath() + "/connections.db";
}

Config &Config::instance() {

    static Config inst__;
    return inst__;
}

QString Config::dataPath() const {

#ifdef __ANDROID__
    QAndroidJniObject mediaDir = QAndroidJniObject::callStaticObjectMethod("android/os/Environment", "getExternalStorageDirectory", "()Ljava/io/File;");
    QAndroidJniObject mediaPath = mediaDir.callObjectMethod("getAbsolutePath", "()Ljava/lang/String;");
    QAndroidJniObject activity = QAndroidJniObject::callStaticObjectMethod("org/qtproject/qt5/android/QtNative", "activity", "()Landroid/app/Activity;");
    QAndroidJniObject package = activity.callObjectMethod("getPackageName", "()Ljava/lang/String;");
    QString dataAbsPath = mediaPath.toString() + "/Android/data/" + package.toString();

    QAndroidJniEnvironment env;
    if(env->ExceptionCheck()) { env->ExceptionClear(); }
    return dataAbsPath;
#else
    QString dataAbsPath = QStandardPaths::writableLocation(QStandardPaths::HomeLocation);
    QString file_name = QCoreApplication::applicationName();

    int idx = file_name.lastIndexOf('.');
    if(idx > 0) {
        file_name = file_name.left(idx);
    }
    dataAbsPath.append(QString("/.%1").arg(file_name));

    return dataAbsPath;
#endif
}

void Config::setDatabaseFile(QString val) {

    m_dbFile = val;
}
