#include "ftpdtp.h"
#include <QLoggingCategory>

Q_LOGGING_CATEGORY(ftpDtp, "ftpDtp", QtDebugMsg)

// ----------------------------------------------------------------------------
FtpDtp::FtpDtp()
    : m_state(Closed)
    , m_mode(FtpDtpMode::None)
    , m_bytesWritten(0)
{
    setObjectName("FtpDtp");
    qRegisterMetaType<QAbstractSocket::SocketError>("QAbstractSocket::SocketError");
    connect(&m_socket, &QIODevice::readyRead,
            this, &FtpDtp::onReadyRead);
    connect(&m_socket, static_cast<void (QAbstractSocket::*)(QAbstractSocket::SocketError)>(&QAbstractSocket::error),
            this, &FtpDtp::onError);
    connect(&m_socket, &QAbstractSocket::stateChanged, this, &FtpDtp::onSocketStateChanged);
    connect(&m_socket, &QAbstractSocket::bytesWritten, this, &FtpDtp::onSocketBytesWritten);
}

// ----------------------------------------------------------------------------
void FtpDtp::setDevice(const QSharedPointer<QIODevice> &val) {

    m_dev = val;
}

// ----------------------------------------------------------------------------
void FtpDtp::setMode(FtpDtpMode val) {

    m_mode = val;
    m_bytesWritten = 0;
}

// ----------------------------------------------------------------------------
void FtpDtp::clearList() {

    if(!m_list.empty()) {
        m_list.clear();
        emit listUpdated();
    }
}

// ----------------------------------------------------------------------------
void FtpDtp::onDtpFileSize(size_t val) {

    m_fileSize = val;
}

// ----------------------------------------------------------------------------
void FtpDtp::onStartWriting() {

    if(!m_dev || !m_socket.isOpen()) {
        emit fileFinished(false);
        return;
    }

    qCDebug(ftpDtp) << QString("start writing");
    setMode(FtpDtpMode::FileWrite);

    if(m_dev->isSequential()) {
        m_dev->seek(0);
    }

    writePortion();
}

// ----------------------------------------------------------------------------
void FtpDtp::writePortion() {

    qint64 maxlen = 1024;
    char buf[1024];
    qint64 sz = m_dev->read(buf, maxlen);

    if(-1 == sz) { // error
        m_socket.close();
    }
    else if(0 == sz) { // end of file
        if(m_dev->size() == (qint64)m_bytesWritten) {
            m_dev.reset(nullptr);
            m_mode = FtpDtpMode::None;
            emit fileFinished(true);
        }
        m_socket.close();
    }
    else {
        m_socket.write(buf, sz);
    }
}

// ----------------------------------------------------------------------------
void FtpDtp::onReadyRead() {

    FtpHelper helper;

    while (m_socket.canReadLine()) {
        if(FtpDtpMode::List == m_mode) {
            const QString line = m_socket.readLine();
//            qCDebug(ftpDtp) << QString("read line: %1").arg(line);
            UrlInfo info;
            if(helper.parseDir(line, info)) {
                m_list.append(info);
            }
        }
        else if(FtpDtpMode::FileRead == m_mode) {
            QByteArray ba;
            ba.resize(m_socket.bytesAvailable());
            qint64 bytesRead = m_socket.read(ba.data(), ba.size());
            if (bytesRead < 0) {
                break;
            }
            ba.resize(bytesRead);
            if(m_dev) {
                m_dev->write(ba);
                m_bytesWritten += bytesRead;
                qCDebug(ftpDtp) << QString("read %1 bytes").arg(m_bytesWritten);
            }
        }
    }

    if(FtpDtpMode::List == m_mode) {
        emit listUpdated();
    }
}

// ----------------------------------------------------------------------------
void FtpDtp::onError(QAbstractSocket::SocketError error_) {

    qCWarning(ftpDtp) << QString("error %1").arg(error_);
}

// ----------------------------------------------------------------------------
void FtpDtp::onSocketStateChanged(QAbstractSocket::SocketState s) {

    switch (s) {
    case QAbstractSocket::ConnectedState:
        m_bytesWritten = 0;
        setState__(Connected);
        break;
    case QAbstractSocket::UnconnectedState:
        if(m_dev) {
            if(FtpDtpMode::FileRead == m_mode) {
                bool rez = true;
                if(m_dev->size() != (qint64)m_fileSize) {
                    qCWarning(ftpDtp) << QString("file size mismatch: actual %1 expected %2")
                                         .arg(m_dev->size()).arg(m_fileSize);
                    rez = false;
                }
                m_dev.reset(nullptr);
                emit fileFinished(rez);
            }
            else if(FtpDtpMode::FileWrite == m_mode) {
                bool rez = true;
                if(m_dev->size() != (qint64)m_bytesWritten) {
                    qCWarning(ftpDtp) << QString("file size mismatch: actual %1 expected %2")
                                         .arg(m_dev->size()).arg(m_fileSize);
                    rez = false;
                }
                m_dev.reset(nullptr);
                emit fileFinished(rez);
            }
            else {
                m_dev.reset(nullptr);
            }
        }
        m_mode = FtpDtpMode::None;
        setState__(Closed);
    default:
        break;
    }
}

// ----------------------------------------------------------------------------
void FtpDtp::onSocketBytesWritten(qint64 bytes) {

    if(FtpDtpMode::FileWrite == m_mode && m_dev) { // continue write data
        m_bytesWritten += bytes;
        qCDebug(ftpDtp) << QString("written %1 bytes").arg(m_bytesWritten);
        writePortion();
    }
}

// ----------------------------------------------------------------------------
void FtpDtp::setState__(State s) {

    if(m_state != s) {
        qCDebug(ftpDtp) << QString("state %1").arg(stateToString(s));
        m_state = s;
        emit stateChanged(m_state);
    }
}

// ----------------------------------------------------------------------------
QString stateToString(FtpDtp::State val) {

    switch (val) {
    case FtpDtp::Connected: return QString("Connected");
    case FtpDtp::Closed: return QString("Closed");
    }

    return QString("[undefined]");
}
