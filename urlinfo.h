#ifndef URLINFO_H
#define URLINFO_H

#include <QString>

class UrlInfo {
public:

    UrlInfo();

    enum Type {
        Dir,
        File,
        Symlink
    };

    const Type &type() const { return m_type; }
    void setType(Type val);
    bool isSymLink() const { return Symlink == m_type; }
    bool isFile() const { return File == m_type; }
    bool isDir() const { return Dir == m_type; }

    QString name() const { return m_name; }
    void setName(QString val);

    QString owner() const { return m_owner; }
    void setOwner(QString val);

    QString group() const { return m_group; }
    void setGroup(QString val);

    qint64 size() const { return m_size; }
    void setSize(qint64 val);

    QString toString() const;

private:

    Type m_type;
    QString m_name;
    QString m_owner;
    QString m_group;
    qint64 m_size;
};

#endif // URLINFO_H
