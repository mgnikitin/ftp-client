#include "core.h"
#include <QUrl>
#include <QStandardPaths>
#include <QFileInfo>
#include <QDir>

// ----------------------------------------------------------------------------
Core::Core()
{
    m_host = "127.0.0.1";
    m_localPath = QStandardPaths::standardLocations(QStandardPaths::HomeLocation).first();

    connect(&m_ftp, &Ftp::commandFinished, this, &Core::onFtpCommandFinished);
}

// ----------------------------------------------------------------------------
Core &Core::instance() {

    static Core inst__;
    return inst__;
}

// ----------------------------------------------------------------------------
void Core::setHost(QString val) {

    if(m_host != val) {
        m_host = val;
        emit hostChanged();
    }
}

// ----------------------------------------------------------------------------
void Core::setUser(QString val) {

    if(m_user != val) {
        m_user = val;
        emit userChanged();
    }
}

// ----------------------------------------------------------------------------
void Core::setPassword(QString val) {

    if(m_pass != val) {
        m_pass = val;
        emit passwordChanged();
    }
}

// ----------------------------------------------------------------------------
void Core::setLocalPath(QString val) {

    if(m_localPath != val) {
        m_localPath = val;
        emit localPathChanged();
    }
}

// ----------------------------------------------------------------------------
void Core::connectToHost() {

    setStatus__("");
    QUrl url(m_host);
    if (!url.isValid() || url.scheme().toLower() != QLatin1String("ftp")) {
        if(m_ftp.connectToHost(m_host)) {
            m_ftp.login(m_user, m_pass);
        }
    }
    else {
        if(m_ftp.connectToHost(url.host(), url.port(21))) {
            m_ftp.login(m_user, m_pass);
        }
    }
}

// ----------------------------------------------------------------------------
void Core::get(QString name) {

    QFileInfo fi;
    fi.setFile(QDir(m_localPath), name);
    if(fi.exists()) {
        setStatus__("File exists");
        emit finished();
        return;
    }

    QFile *file = new QFile(fi.absoluteFilePath());
    if(!file->open(QIODevice::WriteOnly)) {
        setStatus__("Couldn't open file");
        delete file;
        emit finished();
        return;
    }

    m_file.reset(file);
    m_ftp.get(name, qSharedPointerDynamicCast<QIODevice>(m_file), Ftp::Binary);
}

// ----------------------------------------------------------------------------
void Core::send(QString name) {

    QFileInfo fi;
    fi.setFile(QDir(m_localPath), name);
    if(!fi.exists()) {
        setStatus__("File doesn't exist");
        emit finished();
        return;
    }

    QFile *file = new QFile(fi.absoluteFilePath());
    if(!file->open(QIODevice::ReadOnly)) {
        setStatus__("Couldn't open file");
        delete file;
        emit finished();
        return;
    }

    m_file.reset(file);
    m_ftp.send(name, qSharedPointerDynamicCast<QIODevice>(m_file), Ftp::Binary);
}

// ----------------------------------------------------------------------------
void Core::addConnection() {

    if(m_host.isEmpty()) { return; }

    m_connModel.add({m_host, m_host, m_user, m_pass});
}

// ----------------------------------------------------------------------------
void Core::setStatus__(QString val) {

    if(m_status != val) {
        m_status = val;
        emit statusChanged();
    }
}

// ----------------------------------------------------------------------------
void Core::onFtpCommandFinished(FtpCommand c, FtpStatus status) {

//    qDebug() << QString("finished %1: %2").arg(c.toString()).arg(success);

    if(FtpCommand::Login == c.type()) {
        if(FtpStatus::Ok == status) {
            setStatus__(QString::fromUtf8("Login success"));
        }
        else {
            setStatus__(QString::fromUtf8("Login incorrect"));
        }
    }
    else if(FtpCommand::ConnectToHost == c.type()) {
        if(FtpStatus::Ok == status) {
            setStatus__(QString::fromUtf8("Connected"));
        }
        else {
            setStatus__(QString::fromUtf8("Connection error"));
            m_ftp.clear();
        }
        emit connected(FtpStatus::Ok == status);
    }
    else if(FtpCommand::Get == c.type()) {
        if(status != FtpStatus::Ok) {
            if(m_file) {
                m_file->remove();
            }
        }
        if(FtpStatus::FileUnavailable == status) {
            setStatus__(QString("File unavailable"));
        }
        else {
            setStatus__(QString("File %1 downloaded successfully").arg(c.param()));
        }
        m_file.reset(nullptr);
    }
    else if(FtpCommand::Send == c.type()) {
        if(FtpStatus::Ok == status) {
            setStatus__(QString("File %1 uploaded successfully").arg(c.param()));
        }
        else {
            setStatus__(QString("Error with uploading %1").arg(c.param()));
        }
        m_file.reset(nullptr);
    }
    else if(FtpCommand::Cd == c.type()) {
        setStatus__("");
    }

    emit finished();
}
